let salads= [
    {
        "name": "Green salad with tomatoe",
        "prize": "4$",
        "id": 1,
        "ingredients": [
            "Iceberg lettuce",
            "Tomatoes"
        ],
        "imageUrl": "https://farm6.staticflickr.com/5087/5358599242_7251dc7de4.jpg"
    },
    {
        "name": "Tomato salad with mozzarella",
        "prize": "5$",
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella"
        ],
        "imageUrl": "https://farm4.staticflickr.com/3130/5862973974_c107ed81ea.jpg"
    },
    {
        "name": "Field salad with egg",
        "prize": "4$",
        "id": 3,
        "ingredients": [
            "Field salad",
            "Egg"
        ],
        "imageUrl": "https://farm9.staticflickr.com/8223/8372222471_662acd24f6.jpg"
    },
    {
        "name": "Rocket with parmesan",
        "prize": "5$",
        "id": 4,
        "ingredients": [
            "Rocket",
            "Parmesan"
        ],
        "imageUrl": "https://farm8.staticflickr.com/7017/6818343859_bb69394ff2.jpg"
    }
]
salads.forEach(function(salat){//macht eine funktion für jedes Item
    let salatDiv = document.createElement('div');//ein neues DIv wird erstellt um die Items in das zu tun
    salatDiv.classList.add("saladItem");
    salatDiv.innerHTML = '<img class="saladImg" src="' + salat.imageUrl + '"><h4>' + salat.name +'</h4><h4>'+salat.ingredients +'</h4>'
    + salat.prize + '<select name="dressings"><option > italian dressing </option><option >French dressing</option> </select>' +'<button><img  width="20" height="20"  src="Images/shoppingcart.png" ></button>';
    //mit innerHTML wird eigentlich das HTML bearbeitet

document.getElementById("SaladContainer").appendChild(salatDiv);
});
