let Softdrinks = [
    {
        "name": "Coke",
        "prize": "2$",
        "id": 1,
        "imageUrl": "https://farm1.staticflickr.com/71/203324363_b448827eb0.jpg",
        "volume": "50cl"
    },
    {
        "name": "Fanta",
        "prize": "2$",
        "id": 2,
        "imageUrl": "https://farm1.staticflickr.com/684/32876893826_130576f75a.jpg",
        "volume": "50cl"
    },
    {
        "name": "Pepsi",
        "prize": "2$",
        "id": 3,
        "imageUrl": "https://farm4.staticflickr.com/3344/3593103557_bf47c0a3a2.jpg",
        "volume": "50cl"
    },
    {
        "name": "Red bull",
        "prize": "3$",
        "id": 4,
        "imageUrl": "https://farm3.staticflickr.com/2391/2507916617_254348d40c.jpg",
        "volume": "50cl"
    }
]
Softdrinks.forEach(function(softdrink){//macht eine funktion für jedes Item
let softdrinkDiv= document.createElement('div');//ein neues DIv wird erstellt um die Items in das zu tun
softdrinkDiv.classList.add("softdrinksItem");
softdrinkDiv.innerHTML= '<img class="softdrinksimg" src="'+ softdrink.imageUrl + '"><p>' + softdrink.name + '</p><p>' + softdrink.prize + '</p>'+
'<select id="cokeSize"><option value=>330ml</option> <option value=>0.5cl</option> <option value=>1l</option></select>' + '<button> <img width="20" height="20" src="Images/shoppingcart.png"></button>' ;
//mit innerHTML wird eigentlich das HTML bearbeitet
document.getElementById("softDrinksContainer").appendChild(softdrinkDiv);
});