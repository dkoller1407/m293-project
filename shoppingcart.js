document.addEventListener("DOMContentLoaded", function () {
    // ladet den local storage auf 0 oder default
    let cartItemCount = parseInt(localStorage.getItem('cartItemCount')) || 0;


    //reset button
let resetCartCounter= document.getElementById('resetCartCounter')
resetCartCounter.addEventListener('click', function (){
    cartItemCount=0;
    window.alert("Vielen Dank für Ihren Einkauf!");
    cartCounter.textContent = cartItemCount;

    //speichert Zähler in localstorage
    localStorage.setItem('cartItemCount', cartItemCount.toString())

});

    // erneuert den cartcount auf der website
    let cartCounter = document.getElementById("cartCounter");
    cartCounter.textContent = cartItemCount;

    let pizzaIcons = document.querySelectorAll(".pizzaItem button");

    pizzaIcons.forEach(function (pizzaIcon) {
        pizzaIcon.addEventListener("click", function () {
            //der cartcount wird unbestimmmt erhöht
            cartItemCount++;

            // der Cartcount wird nun erneut
            cartCounter.textContent = cartItemCount;

            //der local storage wird nun erneuert
            localStorage.setItem('cartItemCount', cartItemCount.toString());
        });
    });

    // Soft drinks counter              'button' um den button zu wählen und nicht das img
    let softDrinksIcons = document.querySelectorAll(".softdrinksItem button");

    softDrinksIcons.forEach(function (softDrinkIcon) {
        softDrinkIcon.addEventListener("click", function () {
                        //der cartcount wird unbestimmmt erhöht
            cartItemCount++;
            // der Cartcount wird nun erneut

            cartCounter.textContent = cartItemCount;

            //der local storage wird nun erneuert

            localStorage.setItem('cartItemCount', cartItemCount.toString());
        });
    });

    let cartIcons = document.querySelectorAll(".saladItem button");

    cartIcons.forEach(function (cartIcon) {
        cartIcon.addEventListener("click", function () {
            //der cartcount wird unbestimmmt erhöht

            cartItemCount++;
            
               // der Cartcount wird nun geupdatet
                 cartCounter.textContent = cartItemCount;

             //der local storage wird nun erneuert

            localStorage.setItem('cartItemCount', cartItemCount.toString());
        });
    });
});
